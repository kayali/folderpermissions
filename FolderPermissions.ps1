﻿function Get-FolderPermissions {
    param (
        [string]$folderPath
    )

    # Get the Access Control List (ACL) for the folder
    $acl = Get-Acl -Path $folderPath

    # Output the folder path and permissions
    Write-Output "Folder: $folderPath"
    Write-Output "Permissions:"

    foreach ($accessRule in $acl.Access) {
        Write-Output "    $($accessRule.IdentityReference) - $($accessRule.FileSystemRights)"
    }

    Write-Output "---------------------"
}

# The root path for which to list folder permissions
$rootPath = "C:\Path_goes_here"

# Get all sub folders and the root path
$folders = Get-ChildItem -Path $rootPath -Directory -Recurse

# run through each folder and get its permissions
foreach ($folder in $folders) {
    Get-FolderPermissions -folderPath $folder.FullName
}